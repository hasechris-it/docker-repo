version: '3.5'
networks:
  web:
    external: true
  default:
    internal: true

services:
  db:
    image: mariadb:10.2
    container_name: librenms_db
    command:
      - "mysqld"
      - "--sql-mode="
      - "--innodb-file-per-table=1"
      - "--lower-case-table-names=0"
      - "--character-set-server=utf8"
      - "--collation-server=utf8_unicode_ci"
    volumes:
      - "${VOLPATH}/mysql/data:/var/lib/mysql"
    environment:
      - "TZ=${TZ}"
      - "MYSQL_ALLOW_EMPTY_PASSWORD=yes"
      - "MYSQL_DATABASE=${MYSQL_DATABASE}"
      - "MYSQL_USER=${MYSQL_USER}"
      - "MYSQL_PASSWORD=${MYSQL_PASSWORD}"
    restart: always
    networks:
      - default


  memcached:
    image: memcached:alpine
    container_name: librenms_memcached
    environment:
      - "TZ=${TZ}"
    restart: always
    networks:
      - default


  rrdcached:
    image: crazymax/rrdcached
    container_name: librenms_rrdcached
    volumes:
      - "${VOLPATH}/librenms/rrd:/data/db"
      - "${VOLPATH}/rrd-journal:/data/journal"
    environment:
      - "TZ=${TZ}"
      - "LOG_LEVEL=LOG_INFO"
      - "WRITE_TIMEOUT=1800"
      - "WRITE_JITTER=1800"
      - "WRITE_THREADS=4"
      - "FLUSH_DEAD_DATA_INTERVAL=3600"
    restart: always
    networks:
      - default


  smtp:
    image: juanluisbaptiste/postfix
    container_name: librenms_smtp
    volumes:
      - "/etc/localtime:/etc/localtime:ro"
    environment:
      - "SERVER_HOSTNAME=librenms.hasiatthegrill.net"
      - "SMTP_SERVER=${SMTP_SERVER}"
      - "SMTP_USERNAME=${SMTP_USERNAME}"
      - "SMTP_PASSWORD=${SMTP_PASSWORD}"
    restart: always
    networks:
      - web
      - default

  librenms:
    image: librenms/librenms:latest
    container_name: librenms
    domainname: proxy.shared.local
    hostname: librenms
    ports:
      - target: 8000
        published: 8000
        protocol: tcp
    depends_on:
      - db
      - memcached
      - rrdcached
      - smtp
    volumes:
      - "${VOLPATH}/librenms:/data"
    environment:
      - "TZ=${TZ}"
      - "PUID=${PUID}"
      - "PGID=${PGID}"
      - "DB_HOST=db"
      - "DB_NAME=${MYSQL_DATABASE}"
      - "DB_USER=${MYSQL_USER}"
      - "DB_PASSWORD=${MYSQL_PASSWORD}"
      - "DB_TIMEOUT=60"
    env_file:
      - "./librenms.env"
    restart: always
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=web"
      - "traefik.librenmsintern.frontend.rule=Host:librenms.proxy.shared.local"
      - "traefik.frontend.headers.SSLRedirect=true"
      - "traefik.frontend.headers.forceSTSHeader=false"
      - "traefik.frontend.headers.STSSeconds=15552000"
      - "traefik.frontend.headers.referrerPolicy=no-referrer"
      - "traefik.frontend.redirect.permanent=true"
      - "traefik.frontend.redirect.regex=https://(.*)/.well-known/(card|cal)dav"
      - "traefik.frontend.redirect.replacement=https://$$1/remote.php/dav/"
      - "traefik.librenmsextern.frontend.headers.SSLForceHost=librenms.hasiatthegrill.net"
      - "traefik.librenmsextern.frontend.headers.SSLForceHost=true"
      - "traefik.librenmsextern.frontend.rule=Host:librenms.hasiatthegrill.net"
      - "traefik.port=8000"
    networks:
      - web
      - default

  cron:
    image: librenms/librenms:latest
    container_name: librenms_cron
    domainname: proxy.shared.local
    hostname: librenms
    depends_on:
      - librenms
    volumes:
      - "${VOLPATH}/librenms:/data"
    environment:
      - "TZ=${TZ}"
      - "PUID=${PUID}"
      - "PGID=${PGID}"
      - "DB_HOST=db"
      - "DB_NAME=${MYSQL_DATABASE}"
      - "DB_USER=${MYSQL_USER}"
      - "DB_PASSWORD=${MYSQL_PASSWORD}"
      - "DB_TIMEOUT=60"
      - "SIDECAR_CRON=1"
    env_file:
      - "./librenms.env"
    restart: always
    networks:
      - default
      - web
