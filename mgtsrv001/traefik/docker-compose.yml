version: '3'

services:
  traefik:
    container_name: traefik
    # using image tag saintmarcelin for v2.x - no tag for all v2 version available
    image: traefik:saintmarcelin
    restart: always
    command:
      - "--log.level=debug"
      - "--serversTransport.insecureSkipVerify=true"
      - "--entrypoints.web.address=:80"
      - "--entrypoints.websecure.address=:443"
      - "--entrypoints.pbs.address=:8007"
      - "--entrypoints.traefik.address=:8080"
      - "--api=true"
      - "--api.dashboard=true"
      - "--api.insecure=true"
      - "--ping=true"
      - "--ping.entrypoint=traefik"
      - "--providers.docker=true"
      - "--providers.docker.exposedByDefault=false"
      - "--providers.file.directory=/dynamic-config"
      - "--providers.file.watch=true"
      - "--certificatesresolvers.nerdacme.acme.email=hase.christian92@gmail.com"
      - "--certificatesresolvers.nerdacme.acme.storage=/acme/nerd-acme.json"
      - "--certificatesresolvers.nerdacme.acme.caserver=https://acme-v02.api.letsencrypt.org/directory"
      #- "--certificatesresolvers.nerdacme.acme.caserver=https://acme-staging-v02.api.letsencrypt.org/directory"
      - "--certificatesresolvers.nerdacme.acme.httpchallenge=true"
      - "--certificatesresolvers.nerdacme.acme.httpchallenge.entrypoint=web"
      - "--certificatesresolvers.haseacme.acme.email=hase.christian92@gmail.com"
      - "--certificatesresolvers.haseacme.acme.storage=/acme/acme.json"
      - "--certificatesresolvers.haseacme.acme.caserver=https://acme-v02.api.letsencrypt.org/directory"
      #- "--certificatesresolvers.haseacme.acme.caserver=https://acme-staging-v02.api.letsencrypt.org/directory"
      - "--certificatesresolvers.haseacme.acme.dnschallenge=true"
      - "--certificatesresolvers.haseacme.acme.dnschallenge.provider=desec"
      - "--certificatesresolvers.haseacme.acme.dnschallenge.resolvers=ns1.desec.io:53"
      - "--certificatesresolvers.haseacme.acme.dnschallenge.delayBeforeCheck=60s"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - /data_hdd/docker/traefik/traefik/dynamic-config:/dynamic-config:ro
      - /data_hdd/docker/traefik/traefik/acme/:/acme
      - /data_hdd/docker/traefik/traefik/default-cert:/default-cert
    healthcheck:
      test: ["CMD", "traefik", "healthcheck", "--ping"]
      interval: 10s
      timeout: 10s
      retries: 3
    networks:
      docker-vmbr210:
        ipv4_address: 10.253.42.30
      docker-internal:
        aliases:
          - traefik
      proxy-withinternet:
        aliases:
          - traefik

  traefik-default-cert:
    container_name: traefik-default-cert
    image: registry.gitlab.com/hasechris-it/hasechris-docker-images/traefik-default-cert:latest
    volumes:
      # enable execution of docker inside container
      - /var/run/docker.sock:/var/run/docker.sock
      # folder with LE json - also see env variable WATCH_FILE
      - /data_hdd/docker/traefik/traefik/acme:/acme:ro
      # traefik target for extracted cert
      - /data_hdd/docker/traefik/traefik/default-cert:/default-cert
    environment:
      # the file to be watched for changes
      - "WATCH_FILE=/acme/acme.json"
      # domain to extract (MAIN domain, not SAN domain)
      - "CERT_DOMAIN=hasiatthegrill.net"
      # where to copy the cert files (separated by :)
      - "COPY_FULLCHAIN=/default-cert/fullchain.pem"
      - "COPY_PRIVKEY=/default-cert/privkey.pem"
      - "COPY_CHAIN="
      - "COPY_CERT="
      # containers to restart
      - "RESTART=traefik"
    depends_on:
      traefik:
        condition: service_healthy
    restart: always

  whoami:
    image: "traefik/whoami"
    container_name: "whoami"
    depends_on:
    - traefik
    networks:
      docker-internal:
    labels:
      - "traefik.enable=true"
      - "traefik.http.services.whoami.loadbalancer.server.port=80"
      - "traefik.http.services.whoami.loadbalancer.server.scheme=http"
      - "traefik.http.routers.whoami.rule=Host(`hasiatthegrill.net`, `*.hasiatthegrill.net`)"
      - "traefik.http.routers.whoami.entrypoints=websecure"
      - "traefik.http.routers.whoami.tls.certresolver=haseacme"

networks:
  docker-vmbr210:
    external: true
  docker-internal:
    external: true
  proxy-withinternet:
    external: true
  mailcow-network:
    external: true
