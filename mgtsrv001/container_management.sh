#!/bin/bash

function_help() {
  echo "This script will start or stop all compose file via \`docker-compose up -d\` or \`docker-compose down\`"
  echo -e "\nSyntax:\n"
  echo -e "\t-h\tShow this help"
  echo -e "\t--start\tStart specified containers"
  echo -e "\t--stop\tStop specified containers"
}

function_determine_composefile() {
  if [[ -f "$(pwd)/$1/docker-compose.secrets.yml" ]]; then
    echo "-f docker-compose.yml -f docker-compose.secrets.yml"
  else
    echo "-f docker-compose.yml"
  fi
}


####################################
####################################
# list of disabled containers
# dhcp-seniorenwg
list_containers_start="network watchtower dns-hasiatthegrill-seniorenwg traefik dns-proxy-extern dns-proxy-intern unifi pihole heimdall phpipam py-kms laplace nextcloud teamspeak freeradius-servicemanagement freeradius"
list_containers_stop="$(echo $list_containers_start | tr ' ' '\n' | tac | tr '\n' ' ')"

# get parameters
for parameter in "$@"
do
  case $parameter in
    -h | --help)
      function_help
      exit 0
    ;;
    --start)
      for folder in $list_containers_start
      do
	echo "Starting $folder"
        # start containers
        fileargs=$(function_determine_composefile $folder)
        cd $folder
        docker-compose $fileargs up -d
	if [ ! $? -eq 0 ]; then
          echo "Error on startup of $folder. Exiting..."
	  exit 1
	fi
        cd ..
        sleep 3
      done
      ## stop freeradius container, but not remove it
      docker container stop freeradius-accept-all
      break
    ;;
    --stop)
      for folder in $list_containers_stop
      do
        echo "Stopping $folder"
        # stop containers
        fileargs=$(function_determine_composefile $folder)
        cd $folder
        docker-compose $fileargs down
	if [ ! $? -eq 0 ]; then
          echo "Error on stopping of $folder. Exiting..."
	  exit 1
	fi
        cd ..
        sleep 3
      done
      break
    ;;
  esac
done
